## Simple Crypto App

### How to run

- clone, run `yarn install`, `yarn start`

The app is built using Expo which is great especially for trying things out.


There are some differences between the app and the figma file:

- I have used React Native Paper just to try it out and to not "reinvent the wheel", it looks a little different from the designs
- sometimes I deviated from the design because I felt like it made sense (eg. in Figma, the "Swap" button in wallet home is at top of the screen, hard to reach) or because the difference was a minor one


I tried to wire up the Expo app with web3.js to connect to Metamask but I probably need to eject from Expo to be able to make it work, there are some dependencies that likely won't work with Expo because of its limited customization abilities.
I'll take a look at again later and make it work out of curiosity.
