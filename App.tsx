import { StatusBar } from 'expo-status-bar'
import React from 'react'
import { SafeAreaProvider } from 'react-native-safe-area-context'

import useCachedResources from './src/hooks/useCachedResources'
import useColorScheme from './src/hooks/useColorScheme'
import { Navigation } from './src/navigation'
import { AssetsProvider } from './src/swap/AssetsContext/AssetsContext'
import { QueryClientProvider } from 'react-query'
import { getQueryClient } from './src/swap/api/api'

export default function App() {
  const isLoadingComplete = useCachedResources()
  const colorScheme = useColorScheme()

  if (!isLoadingComplete) {
    return null
  } else {
    return (
      <QueryClientProvider client={getQueryClient()}>
        <SafeAreaProvider>
          <AssetsProvider>
            <Navigation colorScheme={colorScheme} />
          </AssetsProvider>
          <StatusBar />
        </SafeAreaProvider>
      </QueryClientProvider>
    )
  }
}
