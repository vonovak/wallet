export default {
  light: {
    text: '#000',
    placeholder: 'grey',
    background: '#fff',
  },
  dark: {
    text: '#fff',
    placeholder: 'grey',
    background: '#000',
  },
}
export const daybreakBlue = '#0185FF'
