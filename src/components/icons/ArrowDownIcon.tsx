import { Ionicons } from '@expo/vector-icons'

import React from 'react'
import type { IconProps } from '@expo/vector-icons/build/createIconSet'

export const ArrowDownIcon = <GLYPHS extends string>(
  props?: Omit<IconProps<GLYPHS>, 'name'>
) => {
  return <Ionicons name="ios-arrow-down" size={24} color="black" {...props} />
}
