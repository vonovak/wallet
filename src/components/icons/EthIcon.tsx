import * as React from 'react'
import Svg, { SvgProps, Path } from 'react-native-svg'
import { defaultIconSize } from './defaultIconSize'

// generated using figma and https://react-svgr.com/playground/?native=true&typescript=true

export function EthIcon(props: SvgProps) {
  return (
    <Svg viewBox="0 0 33 33" {...defaultIconSize} fill="none" {...props}>
      <Path
        d="M16.94 32.408c8.838 0 16.001-7.163 16.001-16 0-8.836-7.163-16-16-16s-16 7.164-16 16c0 8.837 7.163 16 16 16z"
        fill="#F0F0F0"
      />
      <Path d="M17.44 4.408v8.87l7.496 3.35-7.497-12.22z" fill="#000" fillOpacity={0.65} />
      <Path d="M17.439 4.408L9.94 16.628l7.498-3.35v-8.87z" fill="#000" />
      <Path d="M17.44 22.376v6.027l7.501-10.379-7.502 4.352z" fill="#000" fillOpacity={0.65} />
      <Path d="M17.439 28.403v-6.028l-7.498-4.35 7.498 10.378z" fill="#000" />
      <Path d="M17.44 20.981l7.496-4.353-7.497-3.348v7.701z" fill="#000" fillOpacity={0.25} />
      <Path d="M9.94 16.628l7.499 4.353v-7.7L9.94 16.627z" fill="#000" fillOpacity={0.65} />
    </Svg>
  )
}
