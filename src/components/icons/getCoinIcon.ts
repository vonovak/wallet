import { EthIcon } from './EthIcon'
import { UsdcIcon } from './UsdcIcon'
import { SvgProps } from 'react-native-svg'
import React from 'react'
import { WbtcIcon } from './WbtcIcon'
import { Ticker } from '../../swap/AssetsContext/Ticker'

export const getCoinIcon = (ticker: Ticker): React.ComponentType<SvgProps> => {
  const iconMap = {
    [Ticker.ETH]: EthIcon,
    [Ticker.USDC]: UsdcIcon,
    [Ticker.WBTC]: WbtcIcon,
  }
  return iconMap[ticker]
}
