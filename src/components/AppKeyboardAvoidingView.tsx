import { KeyboardAvoidingView, Platform, View, ViewProps } from 'react-native'
import React from 'react'
import { useSafeAreaInsets } from 'react-native-safe-area-context'

// TODO this shall not necessary when https://github.com/facebook/react-native/pull/31402 is resolved

const AppKeyboardAvoidingViewIOS = (props: ViewProps) => {
  const insets = useSafeAreaInsets()

  const offsetThatLooksOkay = 55 + insets.top

  return (
    <KeyboardAvoidingView
      behavior={'padding'}
      keyboardVerticalOffset={offsetThatLooksOkay}
      {...props}
    />
  )
}

const AppKeyboardAvoidingViewAndroid = (props: ViewProps) => React.createElement(View, props)

export const AppKeyboardAvoidingView = Platform.select({
  ios: AppKeyboardAvoidingViewIOS,
  default: AppKeyboardAvoidingViewAndroid,
})
