import React from 'react'

export type ReactElementChildrenProp = {
  children: React.ReactElement
}
