import { ViewStyle } from 'react-native'

export type SpacingProps = number | boolean
export type MaybeViewStyle = ViewStyle | undefined

export type ViewStyleSpacingProps = {
  withSpacingHorizontal?: SpacingProps
  withSpacingVertical?: SpacingProps
  withSpacingTop?: SpacingProps
  withSpacingBottom?: SpacingProps
  withSpacing?: SpacingProps
}

export type ViewStyleDimensionProps = {
  width?: number | `${string}%`
  height?: number | `${string}%`
}

export type ViewStyleDirectionBasedProps = {
  centerVertically?: boolean
  centerHorizontally?: boolean
}

export type ViewStyleCommonFlexProps = {
  spaceBetween?: boolean
  spaceAround?: boolean
}

export type ViewStyleColorProps = {
  backgroundColor?: ViewStyle['backgroundColor']
}

export type ViewStyleCommonProps = ViewStyleDirectionBasedProps &
  ViewStyleCommonFlexProps &
  ViewStyleDimensionProps &
  ViewStyleSpacingProps &
  ViewStyleColorProps
