import { ViewProps } from 'react-native'
import React from 'react'
import { Column } from './Column'

export const ScreenContainer = (props: ViewProps) => (
  <Column {...props} style={[{ flex: 1 }, props.style]} />
)
