import type { ViewStyle } from 'react-native'
import { MaybeViewStyle, ViewStyleSpacingProps } from './types'

export const baseSpacing = 16

export const getSpacingStyles = ({
  withSpacingHorizontal,
  withSpacingVertical,
  withSpacingBottom,
  withSpacingTop,
  withSpacing,
}: ViewStyleSpacingProps): ViewStyle => {
  const paddingHorizontalStyles: MaybeViewStyle = withSpacingHorizontal
    ? { paddingHorizontal: baseSpacing }
    : undefined
  const paddingVerticalStyles: MaybeViewStyle = withSpacingVertical
    ? { paddingVertical: baseSpacing }
    : undefined
  const paddingBottomStyles: MaybeViewStyle = withSpacingBottom
    ? { paddingBottom: baseSpacing }
    : undefined

  const paddingTopStyles: MaybeViewStyle = withSpacingTop
    ? { paddingTop: baseSpacing }
    : undefined

  const paddingStyles: MaybeViewStyle = withSpacing ? { padding: baseSpacing } : undefined

  return {
    ...paddingHorizontalStyles,
    ...paddingVerticalStyles,
    ...paddingBottomStyles,
    ...paddingTopStyles,
    ...paddingStyles,
  }
}
