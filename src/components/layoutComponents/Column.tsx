import { View, ViewProps, ViewStyle } from 'react-native'
import React from 'react'
import { ViewStyleCommonProps } from './types'
import { getSpacingStyles } from './getSpacingStyles'

type MaybeViewStyle = ViewStyle | undefined

const getColumnStyles = ({
  centerVertically,
  centerHorizontally,
  spaceBetween,
  ...spacingStyleProps
}: ViewStyleCommonProps): ViewStyle => {
  const alignItemsParam: ViewStyle['alignItems'] = centerHorizontally ? 'center' : undefined

  const alignItemsStyles: MaybeViewStyle = alignItemsParam
    ? {
        alignItems: alignItemsParam,
      }
    : undefined

  const justifyContentParam: ViewStyle['justifyContent'] = centerVertically
    ? 'center'
    : undefined

  const justifyContentStyles: MaybeViewStyle = justifyContentParam
    ? { justifyContent: justifyContentParam }
    : undefined
  const spaceBetweenStyles: MaybeViewStyle = spaceBetween
    ? { justifyContent: 'space-between' }
    : undefined

  const spacingStyles = getSpacingStyles(spacingStyleProps)
  return {
    ...justifyContentStyles,
    ...alignItemsStyles,
    ...spacingStyles,
    ...spaceBetweenStyles,
  }
}

export const Column = (
  props: ViewStyleCommonProps & Pick<ViewProps, 'style' | 'children'>
) => {
  const styles = getColumnStyles(props)
  return <View {...props} style={[styles, props.style]} />
}
