import { View, ViewProps, ViewStyle } from 'react-native'
import React from 'react'
import { MaybeViewStyle, ViewStyleCommonProps } from './types'
import { getSpacingStyles } from './getSpacingStyles'

const getRowStyles = ({
  centerVertically,
  centerHorizontally,
  spaceBetween,
  ...spacingStyleProps
}: ViewStyleCommonProps): ViewStyle => {
  const justifyContentParam: ViewStyle['justifyContent'] = centerHorizontally
    ? 'center'
    : undefined

  const justifyContentStyles: MaybeViewStyle = justifyContentParam
    ? {
        justifyContent: justifyContentParam,
      }
    : undefined

  const alignItemsParam: ViewStyle['alignItems'] = centerVertically ? 'center' : undefined

  const alignItemStyles: MaybeViewStyle = alignItemsParam
    ? { alignItems: alignItemsParam }
    : undefined

  const spaceBetweenStyles: MaybeViewStyle = spaceBetween
    ? { justifyContent: 'space-between' }
    : undefined

  const spacingStyles = getSpacingStyles(spacingStyleProps)
  return {
    flexDirection: 'row',
    ...justifyContentStyles,
    ...alignItemStyles,
    ...spacingStyles,
    ...spaceBetweenStyles,
  }
}

export const Row = (props: ViewStyleCommonProps & Pick<ViewProps, 'style' | 'children'>) => {
  const styles = getRowStyles(props)
  return <View {...props} style={[styles, props.style]} />
}
