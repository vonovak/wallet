/**
 * If you are not familiar with React Navigation, refer to the "Fundamentals" guide:
 * https://reactnavigation.org/docs/getting-started
 *
 */
import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import * as React from 'react'
import { ColorSchemeName } from 'react-native'
import { RootStackParamList, RootStackScreens } from './types'
import { WalletHomeScreen } from '../swap/WalletHomeScreen'
import { MainSwapScreen } from '../swap/MainSwapScreen'
import { SwapTickerSelectionScreen } from '../swap/SwapTickerSelectionScreen'
import { SwapReviewScreen } from '../swap/SwapReviewScreen'
import { SwapConfirmationScreen } from '../swap/SwapConfirmationScreen'
import { CombinedDarkTheme, CombinedDefaultTheme } from '../components/AppTheme'
import { Provider as PaperProvider } from 'react-native-paper'

export function Navigation({ colorScheme }: { colorScheme: ColorSchemeName }) {
  const theme = colorScheme === 'dark' ? CombinedDarkTheme : CombinedDefaultTheme
  return (
    <PaperProvider theme={theme}>
      <NavigationContainer theme={theme}>
        <RootNavigator />
      </NavigationContainer>
    </PaperProvider>
  )
}

/**
 * A root stack navigator is often used for displaying modals on top of all other content.
 * https://reactnavigation.org/docs/modal
 */
const Stack = createNativeStackNavigator<RootStackParamList>()

const disableBackTitleOption = {
  headerBackTitleVisible: false,
}

function RootNavigator() {
  return (
    <Stack.Navigator>
      {/*<Stack.Screen*/}
      {/*  name={RootStackScreens.ConnectAccount}*/}
      {/*  component={TodoComponent}*/}
      {/*  options={{ ...disableBackTitleOption }}*/}
      {/*/>*/}
      <Stack.Screen
        name={RootStackScreens.WalletHome}
        component={WalletHomeScreen}
        options={{ ...disableBackTitleOption, headerTitle: 'My Funds' }}
      />
      <Stack.Screen
        name={RootStackScreens.MainSwap}
        component={MainSwapScreen}
        options={{ ...disableBackTitleOption, headerTitle: 'Swap' }}
      />
      <Stack.Screen
        name={RootStackScreens.SwapReview}
        component={SwapReviewScreen}
        options={{ ...disableBackTitleOption, title: 'Review Swap' }}
      />
      <Stack.Screen
        name={RootStackScreens.SwapConfirmation}
        component={SwapConfirmationScreen}
        options={{ ...disableBackTitleOption, title: 'Confirm Swap' }}
      />
      <Stack.Group screenOptions={{ presentation: 'modal' }}>
        <Stack.Screen
          name={RootStackScreens.SwapTickerSelection}
          component={SwapTickerSelectionScreen}
        />
      </Stack.Group>
    </Stack.Navigator>
  )
}
