/**
 * Learn more about using TypeScript with React Navigation:
 * https://reactnavigation.org/docs/typescript/
 */

import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { Ticker } from '../swap/AssetsContext/Ticker'

export enum RootStackScreens {
  ConnectAccount = 'ConnectAccount',
  WalletHome = 'WalletHome',
  MainSwap = 'MainSwap',
  SwapTickerSelection = 'SwapTickerSelection',
  SwapReview = 'SwapReview',
  SwapConfirmation = 'SwapConfirmation',
}

export enum SwapSide {
  from = 'from',
  to = 'to',
}

export type MainSwapScreenParams = {
  swapFrom: Ticker
  swapTo: Ticker
}

export type SwapTickerSelectionScreenParams = {
  swapSide: SwapSide
  currentSwapSource: Ticker
  currentSwapTarget: Ticker
}

export type SwapReviewScreenParams = MainSwapScreenParams & {
  sourceAmount: number
  targetAmount: number
}

export type RootStackParamList = {
  [RootStackScreens.ConnectAccount]: undefined
  [RootStackScreens.WalletHome]: undefined
  [RootStackScreens.MainSwap]: MainSwapScreenParams
  [RootStackScreens.SwapTickerSelection]: SwapTickerSelectionScreenParams
  [RootStackScreens.SwapReview]: SwapReviewScreenParams
  [RootStackScreens.SwapConfirmation]: SwapReviewScreenParams
}

export type RootStackScreenProps<Screen extends keyof RootStackParamList> =
  NativeStackScreenProps<RootStackParamList, Screen>
