import React, { useEffect } from 'react'
import { ActivityIndicator } from 'react-native'
import { useForm } from 'react-hook-form'
import {
  extractNumber,
  getSwapAmountRules,
  SwapFieldNames,
  SwapFieldTypes,
} from './utils/swapForm'
import { RootStackScreenProps, RootStackScreens, SwapSide } from '../navigation/types'
import { AmountInput } from './components/AmountInput'
import { useAssetsProvider } from './AssetsContext/AssetsContext'
import { SafeAreaView } from 'react-native-safe-area-context'
import { Button } from 'react-native-paper'
import { usePricingQuery } from './api/api'
import { getQuote } from './utils/getQuote'
import { ArrowDownIcon } from '../components/icons/ArrowDownIcon'
import { Row } from '../components/layoutComponents/Row'
import { Column } from '../components/layoutComponents/Column'
import { daybreakBlue } from '../constants/Colors'
import { AppKeyboardAvoidingView } from '../components/AppKeyboardAvoidingView'

export const MainSwapScreen = ({
  navigation,
  route,
}: RootStackScreenProps<RootStackScreens.MainSwap>) => {
  const { control, handleSubmit, formState, watch, setValue, trigger } =
    useForm<SwapFieldTypes>({
      defaultValues: {
        [SwapFieldNames.swapSourceAmount]: '',
        [SwapFieldNames.swapTargetAmount]: '',
      },
    })
  const { getBalanceForTicker } = useAssetsProvider()
  const { swapFrom, swapTo } = route.params

  const { isLoading, data: pricingData } = usePricingQuery()

  const openSwapSourceSelection = () => {
    navigation.navigate(RootStackScreens.SwapTickerSelection, {
      swapSide: SwapSide.from,
      currentSwapSource: swapFrom,
      currentSwapTarget: swapTo,
    })
  }
  const openSwapTargetSelection = () => {
    navigation.navigate(RootStackScreens.SwapTickerSelection, {
      swapSide: SwapSide.to,
      currentSwapSource: swapFrom,
      currentSwapTarget: swapTo,
    })
  }
  const prefillSourceWithTotalBalance = () => {
    setValue(SwapFieldNames.swapSourceAmount, String(getBalanceForTicker(swapFrom)))
  }

  const gotoReview = (data: SwapFieldTypes) => {
    navigation.navigate(RootStackScreens.SwapReview, {
      swapFrom,
      swapTo,
      sourceAmount: extractNumber(data.swapSourceAmount),
      targetAmount: extractNumber(data.swapTargetAmount),
    })
  }

  const swapSourceAmount = watch(SwapFieldNames.swapSourceAmount)

  useEffect(() => {
    const targetAmount = getQuote({
      pricingData,
      from: swapFrom,
      to: swapTo,
      amount: extractNumber(swapSourceAmount),
    })
    const displayedValue = targetAmount === 0 ? '' : String(targetAmount)
    setValue(SwapFieldNames.swapTargetAmount, displayedValue)
    trigger()
  }, [pricingData, setValue, swapFrom, swapSourceAmount, swapTo, trigger])

  if (!pricingData || isLoading) {
    // TODO handle error state
    return <ActivityIndicator />
  }

  const swapEnabled = formState.isValid && swapSourceAmount.length > 0
  return (
    <SafeAreaView edges={['bottom']} style={{ flex: 1, padding: 16 }}>
      <AppKeyboardAvoidingView style={{ flex: 1, justifyContent: 'space-between' }}>
        <Column>
          <AmountInput<SwapFieldTypes, SwapFieldNames.swapSourceAmount>
            name={SwapFieldNames.swapSourceAmount}
            fieldError={formState.errors?.swapSourceAmount}
            control={control}
            prefillWithMaxAmount={prefillSourceWithTotalBalance}
            balance={getBalanceForTicker(swapFrom)}
            rules={getSwapAmountRules(getBalanceForTicker(swapFrom))}
            ticker={swapFrom}
            topLeftLabel={'From'}
            onTickerPress={openSwapSourceSelection}
          />
          <Row centerHorizontally withSpacing>
            <ArrowDownIcon color={daybreakBlue} />
          </Row>
          <AmountInput<SwapFieldTypes, SwapFieldNames.swapTargetAmount>
            name={SwapFieldNames.swapTargetAmount}
            inputEnabled={false}
            control={control}
            balance={getBalanceForTicker(swapTo)}
            ticker={swapTo}
            topLeftLabel={'To'}
            fieldError={formState.errors?.swapTargetAmount}
            rules={{
              validate: () => {
                return swapFrom !== swapTo
              },
            }}
            onTickerPress={openSwapTargetSelection}
          />
        </Column>
        <Button
          mode="contained"
          color={daybreakBlue}
          disabled={!swapEnabled}
          onPress={handleSubmit(gotoReview)}
        >
          Review
        </Button>
      </AppKeyboardAvoidingView>
    </SafeAreaView>
  )
}
