import React from 'react'
import { Button, Caption, Divider, Headline, Subheading } from 'react-native-paper'
import { SwapReviewScreenParams } from '../navigation/types'
import { getCoinAmountDescriptor } from './utils/getCoinAmountDescriptor'
import { SafeAreaView } from 'react-native-safe-area-context'
import { View } from 'react-native'
import { Row } from '../components/layoutComponents/Row'
import { ArrowDownIcon } from '../components/icons/ArrowDownIcon'
import { Column } from '../components/layoutComponents/Column'
import { defaultIconSize } from '../components/icons/defaultIconSize'
import { Ticker } from './AssetsContext/Ticker'
import { daybreakBlue } from '../constants/Colors'

export const SwapReviewAndConfirmation = ({
  buttonLabel,
  onSwapButtonPress,
  swapFrom,
  swapTo,
  sourceAmount,
  targetAmount,
}: SwapReviewScreenParams & {
  buttonLabel: string
  onSwapButtonPress: () => void
}) => {
  return (
    <SafeAreaView edges={['bottom']} style={{ flex: 1 }}>
      <Column spaceBetween style={{ flex: 1 }} withSpacingTop>
        <View>
          <AssetRow amount={sourceAmount} ticker={swapFrom} />

          <Row withSpacing>
            <Row width={defaultIconSize.width} centerHorizontally>
              <ArrowDownIcon />
            </Row>
          </Row>

          <AssetRow amount={targetAmount} ticker={swapTo} />

          <Row withSpacing>
            <Subheading>Details</Subheading>
          </Row>
          <Divider />

          <TransactionMetadataRow
            title="Minimum received"
            value={getCoinAmountDescriptor(targetAmount * 0.9, swapTo).amountLabel}
          />
          <Divider />

          <TransactionMetadataRow
            title="Network & Protocol fees"
            value={getCoinAmountDescriptor(targetAmount * 0.1, swapTo).amountLabel}
          />

          <Divider />
        </View>
        <Column withSpacing>
          <Button mode="contained" color={daybreakBlue} onPress={onSwapButtonPress}>
            {buttonLabel}
          </Button>
        </Column>
      </Column>
    </SafeAreaView>
  )
}

type AssetRowProps = {
  ticker: Ticker
  amount: number
}
const AssetRow = ({ ticker, amount }: AssetRowProps) => {
  const { CoinIcon } = getCoinAmountDescriptor(amount, ticker)

  return (
    <Row spaceBetween withSpacingHorizontal centerVertically>
      <Row centerVertically style={{ flex: 1 }}>
        <CoinIcon />
        <Row width={11} />
        <Headline>{amount}</Headline>
      </Row>
      <Subheading>{ticker}</Subheading>
    </Row>
  )
}

const TransactionMetadataRow = ({ title, value }: { title: string; value: string }) => {
  return (
    <Row withSpacing spaceBetween>
      <Caption>{title}</Caption>
      <Caption>{value}</Caption>
    </Row>
  )
}
