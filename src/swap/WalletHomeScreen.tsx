import React from 'react'
import { Button, Subheading, List, Divider } from 'react-native-paper'
import { View } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context'
import { RootStackScreenProps, RootStackScreens } from '../navigation/types'
import { useAssetsProvider } from './AssetsContext/AssetsContext'
import { Ticker } from './AssetsContext/Ticker'
import { usePrefetchPricingData } from './api/api'
import { getCoinAmountDescriptor } from './utils/getCoinAmountDescriptor'
import { Row } from '../components/layoutComponents/Row'
import { Column } from '../components/layoutComponents/Column'
import { daybreakBlue } from '../constants/Colors'

export const WalletHomeScreen = ({
  navigation,
}: RootStackScreenProps<RootStackScreens.WalletHome>) => {
  const { assets } = useAssetsProvider()
  usePrefetchPricingData()

  return (
    <SafeAreaView edges={['bottom']} style={{ flex: 1 }}>
      <Column withSpacing style={{ flex: 1, justifyContent: 'space-between' }}>
        <View>
          {assets.map(({ ticker, amount, name }) => {
            const { amountLabel, CoinIcon } = getCoinAmountDescriptor(amount, ticker)
            return (
              <React.Fragment key={ticker}>
                <List.Item
                  title={<Subheading>{name}</Subheading>}
                  left={(_props) => (
                    <Row centerVertically>
                      <CoinIcon />
                    </Row>
                  )}
                  right={(_props) => (
                    <Row centerVertically>
                      <Subheading>{amountLabel}</Subheading>
                    </Row>
                  )}
                />
                <Divider />
              </React.Fragment>
            )
          })}
        </View>

        <Button
          mode="contained"
          color={daybreakBlue}
          onPress={() =>
            navigation.navigate(RootStackScreens.MainSwap, {
              swapFrom: Ticker.USDC,
              swapTo: Ticker.ETH,
            })
          }
        >
          Swap
        </Button>
      </Column>
    </SafeAreaView>
  )
}
