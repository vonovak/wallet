import { Ticker } from '../AssetsContext/Ticker'
import { SimpleApi } from '../../api/coingecko/generated'
import { QueryClient, useQuery } from 'react-query'

const geckoSimpleApi = new SimpleApi()

export type PricingData = { [key in Ticker]: number }

const queryClient = new QueryClient()

export const getQueryClient = () => queryClient

export async function getPricesVsUSD(): Promise<PricingData> {
  const usdcId = 'usd-coin'
  const ethId = 'ethereum'
  const wbtcId = 'wrapped-bitcoin'

  // unfortunately, CG's api isn't very well-structured so the generated code had to be adjusted manually
  const response = await geckoSimpleApi.simplePriceGet({
    ids: `${ethId},${wbtcId},${usdcId}`,
    vsCurrencies: 'USD',
  })
  return {
    [Ticker.ETH]: response[ethId].usd,
    [Ticker.WBTC]: response[wbtcId].usd,
    [Ticker.USDC]: response[usdcId].usd,
  }
}

export const getPricingQueryKey = 'getPrices'

export const usePricingQuery = (onSuccess?: (data: PricingData) => void) => {
  const query = useQuery(getPricingQueryKey, getPricesVsUSD, { onSuccess })
  return query
}

export const usePrefetchPricingData = () => {
  return queryClient.prefetchQuery(getPricingQueryKey, getPricesVsUSD)
}
