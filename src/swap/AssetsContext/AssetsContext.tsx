import React, { useContext, useState } from 'react'
import { ReactElementChildrenProp } from '../../components/types'
import { getFullCurrencyName, Ticker } from './Ticker'
import type { SwapReviewScreenParams } from '../../navigation/types'
import produce from 'immer'

export type Asset = {
  ticker: Ticker
  amount: number
  name: string
}

const initialAssets: ReadonlyArray<Asset> = [
  {
    ticker: Ticker.USDC,
    amount: 3500,
  },
  {
    ticker: Ticker.WBTC,
    amount: 0.00027272,
  },
  {
    ticker: Ticker.ETH,
    amount: 5.2489,
  },
].map((it) => ({ ...it, name: getFullCurrencyName(it.ticker) }))

type AssetsContextValue = Readonly<{
  assets: ReadonlyArray<Asset>
  getBalanceForTicker: (ticker: Ticker) => number
  updateBalances: (params: SwapReviewScreenParams) => void
}>

const warnOutsideOfContext = () => {
  console.error('you have used AssetsContext outside of its Provider; this is a noop')
}

const AssetsContext = React.createContext<AssetsContextValue>({
  assets: [],
  updateBalances: warnOutsideOfContext,
  getBalanceForTicker: () => {
    warnOutsideOfContext()
    return 0
  },
})

export const AssetsProvider = ({ children }: ReactElementChildrenProp) => {
  const [assets, setAssets] = useState(initialAssets)
  const getBalanceForTicker = (ticker: Ticker): number => {
    const tickerEntry = assets.find((it) => it.ticker === ticker)
    const balance = tickerEntry ? tickerEntry.amount : 0
    return balance
  }
  const updateBalances = ({
    sourceAmount,
    swapFrom,
    swapTo,
    targetAmount,
  }: SwapReviewScreenParams) => {
    setAssets(
      produce((draft) => {
        const sourceAsset = draft.find((it) => it.ticker === swapFrom)
        const targetAsset = draft.find((it) => it.ticker === swapTo)
        if (targetAsset && sourceAsset) {
          sourceAsset.amount = Math.max(0, sourceAsset.amount - sourceAmount)
          targetAsset.amount = Math.max(0, targetAsset.amount + targetAmount)
        }
      })
    )
  }

  return (
    <AssetsContext.Provider value={{ assets, getBalanceForTicker, updateBalances }}>
      {children}
    </AssetsContext.Provider>
  )
}

export const useAssetsProvider = (): AssetsContextValue => {
  const ctx = useContext(AssetsContext)
  return ctx
}
