export enum Ticker {
  ETH = 'ETH',
  USDC = 'USDC',
  WBTC = 'WBTC',
}

export const getFullCurrencyName = (ticker: Ticker): string => {
  const map = {
    [Ticker.ETH]: 'Ethereum',
    [Ticker.USDC]: 'USDC',
    [Ticker.WBTC]: 'Wrapped Bitcoin',
  }
  return map[ticker]
}
