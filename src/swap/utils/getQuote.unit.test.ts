import { getQuote } from './getQuote'
import { Ticker } from '../AssetsContext/Ticker'

const pricingData = {
  ETH: 4071.51,
  USDC: 0.999924,
  WBTC: 60863,
}

it.each([
  // naive cases
  { from: Ticker.USDC, to: Ticker.USDC, amount: 10, expected: 10 },
  { from: Ticker.ETH, to: Ticker.ETH, amount: 10, expected: 10 },
  { from: Ticker.WBTC, to: Ticker.WBTC, amount: 10, expected: 10 },

  { from: Ticker.USDC, to: Ticker.ETH, amount: pricingData.ETH, expected: pricingData.USDC },

  {
    from: Ticker.ETH,
    to: Ticker.USDC,
    amount: 5,
    expected: pricingData.ETH * (5 / pricingData.USDC),
  },

  {
    from: Ticker.USDC,
    to: Ticker.WBTC,
    amount: pricingData.WBTC * 2,
    expected: pricingData.WBTC * 2 * pricingData.USDC,
  },

  {
    from: Ticker.WBTC,
    to: Ticker.USDC,
    amount: 10,
    expected: pricingData.WBTC * (10 / pricingData.USDC),
  },

  {
    from: Ticker.WBTC,
    to: Ticker.ETH,
    amount: 5,
    expected: (pricingData.WBTC * 5) / pricingData.ETH,
  },

  {
    from: Ticker.ETH,
    to: Ticker.WBTC,
    amount: 5,
    expected: (pricingData.ETH * 5) / pricingData.WBTC,
  },
])('.getQuote(%s)', ({ from, to, amount, expected }) => {
  expect(getQuote({ pricingData, from, to, amount })).toBe(expected)
})
