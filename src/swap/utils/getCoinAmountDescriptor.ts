import { getCoinIcon } from '../../components/icons/getCoinIcon'
import { Ticker } from '../AssetsContext/Ticker'

export const getCoinAmountDescriptor = (amount: number, ticker: Ticker) => {
  const CoinIcon = getCoinIcon(ticker)
  const nbsp = '\u00A0'
  const amountLabel = `${amount}${nbsp}${ticker}`
  return {
    CoinIcon,
    amountLabel,
  }
}
