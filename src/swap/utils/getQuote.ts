import { Ticker } from '../AssetsContext/Ticker'
import { PricingData } from '../api/api'

type Params = {
  pricingData?: PricingData
  amount: number
  from: Ticker
  to: Ticker
}
export const getQuote = ({ pricingData, from, to, amount }: Params): number => {
  if (!pricingData) {
    return 0
  }
  if (from === to) {
    return amount
  }
  // TODO we should use api or bigInts for currency operations
  const sourceInRealUSD = pricingData[from] * amount
  const valueInTermsOfTarget = sourceInRealUSD / pricingData[to]
  return valueInTermsOfTarget
}
