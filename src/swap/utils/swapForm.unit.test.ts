import { getSwapAmountRules } from './swapForm'

const tokenBalance = 10

describe('getSwapAmountRules', () => {
  it.each([
    ['', true],
    ['adfsd', false],
    ['-1', false],
    ['0', false],
    ['0.01', true],
    ['9.99', true],
    ['10', true],
    ['10.001', false],
  ])(
    'given a balance of ' + tokenBalance + ', validate(%s) returns %s',
    (inputValue, expected) => {
      const rules = getSwapAmountRules(tokenBalance)
      expect(rules.validate).toBeInstanceOf(Function)
      // @ts-ignore cannot invoke undefined
      expect(rules.validate(inputValue)).toBe(expected)
    }
  )
})
