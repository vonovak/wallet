import { RegisterOptions } from 'react-hook-form'

export enum SwapFieldNames {
  swapSourceAmount = 'swapSourceAmount',
  swapTargetAmount = 'swapTargetAmount',
}

export type SwapFieldTypes = {
  [value in SwapFieldNames]: string
}

export const getSwapAmountRules = (balance: number): RegisterOptions => {
  return {
    validate: (value) => {
      if (value === undefined || value === '') {
        // we consider an empty input as a valid one, to not annoy users
        return true
      }

      const extractedNumber = extractNumber(value)
      const isGreaterThanZero = extractedNumber > 0
      const isWithinAvailableBalance = extractedNumber <= balance
      return isGreaterThanZero && isWithinAvailableBalance
    },
  }
}

export const extractNumber = (value: string | number, defaultValue = 0) => {
  const valueAsNumber = Number(value)
  const extractedNumber = Number.isFinite(valueAsNumber) ? valueAsNumber : defaultValue
  return extractedNumber
}
