import React, { useLayoutEffect } from 'react'
import { ScreenContainer } from '../components/layoutComponents/LayoutComponents'
import {
  MainSwapScreenParams,
  RootStackScreenProps,
  RootStackScreens,
  SwapSide,
} from '../navigation/types'
import { FlatList } from 'react-native'
import { Asset, useAssetsProvider } from './AssetsContext/AssetsContext'
import { Divider, List, Searchbar, Subheading } from 'react-native-paper'
import { Ticker } from './AssetsContext/Ticker'
import Fuse from 'fuse.js'
import { getCoinAmountDescriptor } from './utils/getCoinAmountDescriptor'
import { Row } from '../components/layoutComponents/Row'
import { Octicons } from '@expo/vector-icons'

export const SwapTickerSelectionScreen = ({
  route,
  navigation,
}: RootStackScreenProps<RootStackScreens.SwapTickerSelection>) => {
  const { swapSide, currentSwapSource, currentSwapTarget } = route.params

  useLayoutEffect(() => {
    const screenTitle = swapSide === SwapSide.from ? 'Swap from' : 'Swap to'
    navigation.setOptions({
      title: screenTitle,
    })
  }, [navigation, swapSide])

  const { searchQuery, setSearchQuery, userFilteredAssets } = useFilteredAssets()

  const assets = (() => {
    const isChoosingSwapSource = swapSide === SwapSide.from
    if (isChoosingSwapSource) {
      return userFilteredAssets
    } else {
      // user is choosing swap target:
      // do not offer swapping from source to source, it makes no sense
      return userFilteredAssets.filter((it) => it.ticker !== currentSwapSource)
    }
  })()

  return (
    <ScreenContainer>
      <Searchbar placeholder="Search" onChangeText={setSearchQuery} value={searchQuery} />
      <FlatList<Asset>
        data={assets}
        keyExtractor={(it) => it.ticker}
        ItemSeparatorComponent={Divider}
        keyboardShouldPersistTaps={'handled'}
        renderItem={({ item }) => (
          <ListItem
            item={item}
            swapSide={swapSide}
            currentSwapTarget={currentSwapTarget}
            currentSwapSource={currentSwapSource}
            navigation={navigation}
          />
        )}
      />
    </ScreenContainer>
  )
}

const useFilteredAssets = () => {
  const { assets } = useAssetsProvider()

  const [searchQuery, setSearchQuery] = React.useState('')

  const userFilteredAssets: ReadonlyArray<Asset> = (() => {
    if (searchQuery) {
      const keys: Array<keyof Asset> = ['ticker', 'name']
      const options = {
        isCaseSensitive: false,
        keys,
      }
      const fuse = new Fuse(assets, options)

      return fuse.search(searchQuery).map((it) => it.item)
    }
    return assets
  })()
  return {
    searchQuery,
    setSearchQuery,
    userFilteredAssets,
  }
}

type ListItemProps = {
  item: Asset
  swapSide: SwapSide
  currentSwapTarget: Ticker
  currentSwapSource: Ticker
  navigation: RootStackScreenProps<RootStackScreens.SwapTickerSelection>['navigation']
}
const ListItem = ({
  item,
  swapSide,
  currentSwapTarget,
  currentSwapSource,
  navigation,
}: ListItemProps) => {
  const { ticker, amount, name } = item
  const { CoinIcon, amountLabel } = getCoinAmountDescriptor(amount, ticker)

  const setSwapSide = () => {
    const params: MainSwapScreenParams =
      swapSide === SwapSide.from
        ? { swapFrom: ticker, swapTo: currentSwapTarget }
        : { swapFrom: currentSwapSource, swapTo: ticker }
    navigation.navigate(RootStackScreens.MainSwap, params)
  }

  return (
    <List.Item
      onPress={setSwapSide}
      title={name}
      left={() => <CoinIcon />}
      right={() => (
        <Row centerVertically>
          <Subheading>{amountLabel}</Subheading>
          <Row width={10} />
          <Octicons name="chevron-right" size={12} color="black" />
        </Row>
      )}
    />
  )
}
