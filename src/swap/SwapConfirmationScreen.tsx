import { RootStackScreenProps, RootStackScreens } from '../navigation/types'
import { useAssetsProvider } from './AssetsContext/AssetsContext'
import { SwapReviewAndConfirmation } from './SwapReviewAndConfirmation'
import * as React from 'react'

export function SwapConfirmationScreen(
  props: RootStackScreenProps<RootStackScreens.SwapConfirmation>
) {
  const { updateBalances } = useAssetsProvider()
  const { swapFrom, swapTo, sourceAmount, targetAmount } = props.route.params

  const executeSwap = () => {
    updateBalances({
      sourceAmount,
      swapFrom,
      swapTo,
      targetAmount,
    })
    props.navigation.popToTop()
  }
  return (
    <SwapReviewAndConfirmation
      {...props.route.params}
      buttonLabel="Confirm Swap"
      onSwapButtonPress={executeSwap}
    />
  )
}
