import React from 'react'
import { FieldError, FieldPath, FieldValues } from 'react-hook-form'
import { FormInput, FormInputProps } from './FormInput'
import { Caption, Text } from 'react-native-paper'
import { getCoinIcon } from '../../components/icons/getCoinIcon'
import { MaterialIcons } from '@expo/vector-icons'
import { TouchableWithoutFeedback } from 'react-native'
import { Ticker } from '../AssetsContext/Ticker'
import { MaxButton } from './MaxButton'
import { Row } from '../../components/layoutComponents/Row'
import { Column } from '../../components/layoutComponents/Column'

type Props<TFieldValues extends FieldValues, TName extends FieldPath<TFieldValues>> = {
  topLeftLabel: string
  balance: number
  ticker: Ticker
  prefillWithMaxAmount?: () => void
  onTickerPress: () => void
  // formState: FormState<TFieldValues>
  fieldError?: FieldError
  inputEnabled?: boolean
} & FormInputProps<TFieldValues, TName>

const tickerIconSize = {
  width: 22,
  height: 22,
}

export function AmountInput<
  TFieldValues extends FieldValues,
  TName extends FieldPath<TFieldValues>
>({
  control,
  name,
  topLeftLabel,
  ticker,
  fieldError,
  balance,
  rules,
  prefillWithMaxAmount,
  inputEnabled = true,
  onTickerPress,
}: Props<TFieldValues, TName>) {
  const borderColor = fieldError ? 'red' : '#D9D9D9'

  const CoinIcon = getCoinIcon(ticker)
  return (
    <Column withSpacing={10} style={{ borderWidth: 1, borderColor, borderRadius: 5 }}>
      <Caption>{topLeftLabel}</Caption>

      <Row centerHorizontally>
        <Row
          spaceBetween
          style={{
            flex: 3,
          }}
        >
          <FormInput
            control={control}
            name={name}
            rules={rules}
            placeholder={'0.00'} // TODO use Intl to format
            style={{ fontWeight: '600', fontSize: 20, flex: 1 }}
            editable={inputEnabled}
            keyboardType={'decimal-pad'}
          />
          {!!prefillWithMaxAmount && <MaxButton onPress={prefillWithMaxAmount} />}
        </Row>

        <Row width={11} />

        <TouchableWithoutFeedback onPress={onTickerPress}>
          <Row
            centerVertically
            spaceBetween
            style={{
              flex: 1,
            }}
          >
            <Text style={{ fontWeight: '500', fontSize: 14, lineHeight: 22, minWidth: 40 }}>
              {ticker}
            </Text>
            <CoinIcon {...tickerIconSize} />
            <MaterialIcons name="keyboard-arrow-down" size={15} color="black" />
          </Row>
        </TouchableWithoutFeedback>
      </Row>

      <Row style={{ justifyContent: 'flex-end' }}>
        <Caption>Balance {balance}</Caption>
      </Row>
    </Column>
  )
}
