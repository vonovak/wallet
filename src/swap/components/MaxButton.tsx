import React from 'react'
import { Text } from 'react-native-paper'
import { TouchableHighlight, TouchableOpacity } from 'react-native'
import { daybreakBlue } from '../../constants/Colors'

type Props = {
  onPress: () => void
}
export const MaxButton = ({ onPress }: Props) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        width: 45,
        height: 25,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#1874FF',
        borderWidth: 1,
        borderRadius: 5,
      }}
    >
      <Text style={{ color: daybreakBlue }}>MAX</Text>
    </TouchableOpacity>
  )
}
