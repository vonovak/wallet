import React from 'react'
import {
  Control,
  Controller,
  FieldPath,
  FieldValues,
  PathValue,
  UnpackNestedValue,
  UseControllerProps,
} from 'react-hook-form'
import { TextInput, TextInputProps } from 'react-native'
import { useThemeColor } from '../../components/Themed'

export type FormInputProps<
  TFieldValues extends FieldValues,
  TName extends FieldPath<TFieldValues>
> = {
  control: Control<TFieldValues>
  name: TName
  defaultValue?: UnpackNestedValue<PathValue<TFieldValues, TName>>
  rules?: UseControllerProps<TFieldValues, TName>['rules']
} & Omit<TextInputProps, 'value' | 'onChange' | 'onChangeText' | 'onBlur'>

export function FormInput<
  TFieldValues extends FieldValues,
  TName extends FieldPath<TFieldValues>
>({ rules, control, name, defaultValue, ...other }: FormInputProps<TFieldValues, TName>) {
  const textColor = useThemeColor('text')
  const placeholderColor = useThemeColor('placeholder')

  return (
    <Controller
      control={control}
      name={name}
      rules={rules}
      defaultValue={defaultValue}
      render={function FormInputControllerRenderProp({ field, fieldState: _fieldState }) {
        return (
          <TextInput
            {...other}
            style={[{ color: textColor }, other.style]}
            placeholderTextColor={placeholderColor}
            value={field.value}
            onBlur={field.onBlur}
            onChangeText={field.onChange}
          />
        )
      }}
    />
  )
}
