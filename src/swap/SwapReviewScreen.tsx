import { RootStackScreenProps, RootStackScreens } from '../navigation/types'
import { Alert } from 'react-native'
import { SwapReviewAndConfirmation } from './SwapReviewAndConfirmation'
import * as React from 'react'

export function SwapReviewScreen(props: RootStackScreenProps<RootStackScreens.SwapReview>) {
  const gotToSwapConfirmation = () => {
    Alert.alert('[Wallet]', 'Do you want to approve the swap?', [
      {
        text: 'Cancel',
        style: 'cancel',
      },
      {
        text: 'OK',
        onPress: () =>
          props.navigation.replace(RootStackScreens.SwapConfirmation, props.route.params),
      },
    ])
  }
  return (
    <SwapReviewAndConfirmation
      {...props.route.params}
      buttonLabel="Approve Swap"
      onSwapButtonPress={gotToSwapConfirmation}
    />
  )
}
